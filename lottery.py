import flask
import json, random, datetime

server= flask.Flask(__name__)

# 初始化
holiday_num = 7
v_start_date = '2021-02-11'
data_filepath='./lottery.namelist.txt'

name_list = []
lucky_list = []

# 开始页面
@server.route('/start')
def startUp():
    v_html = '<a href="/loaddata"> Load Name List </a>'
    return v_html

# 加载本地数据
@server.route('/loaddata')
def loadData():
    try:

        # 读取文件
        datafile = open(data_filepath, 'r', encoding='utf8')
        v_html = '<table><tr><td><ol>'

        for v_name in datafile:
            name_list.append(v_name.strip("\n"))
            v_html += '<li>'+v_name+'</li>'

        v_html += "</ol></td><td><a href='/lottery'> >>> START <<< </a></td><tr></table>"

        return v_html
    except:
        return '<a href="/loaddata"> Load Name List </a>'

@server.route('/lottery')
def getLottery():
    try:
        # 判断是否已产生结果
        # 如果列表为空, 则重新开始
        if len( lucky_list ) == 0:
            # 读取列表后, 随机打乱
            random.shuffle(name_list)
            # 打印乱序列表
            print( name_list )

            v_seq = 0
            v_date = v_start_date
            # 开始循环, 按顺序读取乱序后列表
            while( v_seq < holiday_num):

                luck_person=dict( luckname=name_list[v_seq], luckday=v_date )
                lucky_list.append(luck_person)
                # 生成对应日期
                v_date_fmt = datetime.datetime.strptime(v_date, "%Y-%m-%d") + datetime.timedelta(days=1)
                v_date = v_date_fmt.strftime('%Y-%m-%d')

                v_seq += 1
        else:
            pass

        # 页面输出结果
        v_html = '<ol>'

        for v_lucky in lucky_list:
            v_lucky_one = json.dumps(v_lucky,  ensure_ascii=False)
            v_html += '<li>'+ v_lucky_one + '</li>'
            print(v_lucky_one)

        v_html += '</ol>'

        return v_html
    except:
        return '<a href="/blank"> 马上开奖 </a>'

# 跳转空白页面
@server.route('/blank')
def waitLottery():
    return '<div><a href="/lottery"> 点击查看结果 </a></div>'

# 启动服务
server.run(port=7777,debug=True,host='0.0.0.0')